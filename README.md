# Requirements

- Matreshka League libraries : http://forge.ada-ru.org/matreshka/
- utilada compiled with CURL support : https://github.com/stcarrez/ada-util/
- Ada Console Utils : https://bitbucket.org/cnngimenez/ada-console-utils/

# Install
Compile and install with the usual commands.

```
make
make install
```

Makefile has more rules. Check the Makefile file for more information.

# Config
Configuration files are at ./config directory. These files are used by the tool binaries at ./bin. 

- ./config/token.conf : A simple text file with your bot Token. This token is provided by Telegram's BotFather when requesing a new bot.

# License
Copyright 2020 Christian Gimenez.
This work is under the GPL version 3.
