### Makefile --- 

## Author: cnngimenez
## Version: $Id: Makefile,v 0.0 2020/01/27 20:08:00  Exp $
## Keywords: 
## X-URL: 

# Copyright 2020 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Library type "relocatable"/"dynamic" or "static"
LIBRARY_KIND=dynamic
# Prefix install path (where to install).
PREFIX=$(HOME)/Ada/installs

## Rules
compile: libs tools

all: compile install

libs:
	@echo "Compiling libraries as $(LIBRARY_KIND) type"
	gprbuild telegram.gpr
tools:
	gprbuild telegram_tools.gpr

install: uninstall
	@echo Installing into $(PREFIX)
	gprinstall -p --prefix=$(PREFIX) telegram.gpr
	gprinstall -p --prefix=$(PREFIX) telegram_tools.gpr

uninstall:
	@echo Uninstalling from $(PREFIX)
	-gprinstall --prefix=$(PREFIX) --uninstall telegram.gpr
	-gprinstall --prefix=$(PREFIX) --uninstall telegram_tools.gpr

clean:
	gprclean telegram.gpr
	gprclean telegram_tools.gpr

params:
	@echo Install into: $(PREFIX)
	@echo Library kind (dynamic/static): $(LIBRARY_KIND)

### Makefile ends here
