--  inline_keyboard.adb ---

--  Copyright 2020 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
use Ada.Text_Io;
--  with Ada.Wide_Wide_Text_Io;

with League.Json.Documents;
use League.Json.Documents;
with League.Strings;
use League.Strings;

with Telegram.Data;
use Telegram.Data;
with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Bots;
--  no use telegram.bots needed
with Telegram.Puts;
use Telegram.Puts;

with Get_Token;

procedure Inline_Keyboard is
    
    function Ww2us (S : Wide_Wide_String) return Universal_String 
      renames League.Strings.To_Universal_String;
    
    Inline_Keyboard : Inline_Keyboard_Vectors.Vector;
    
    procedure Build_Keyboard is
        Button : Inline_Button_Type;
    begin
        Button.Text := Ww2us ("Button 1");
        Button.Callback_Data := Ww2us ("btn1");
        Inline_Keyboard.Append (Button);
        
        Button.Text := Ww2us ("Button 2");
        Button.Callback_Data := Ww2us ("btn2");
        Inline_Keyboard.Append (Button);        

    end Build_Keyboard;
    
    procedure My_Update_Received (Connection : Connection_Type;
                                  Update : Update_Type) is
        Json_Doc : Json_Document;
    begin
        Put_Line ("Update received...");
        Put (Update);
    end My_Update_Received;
    
    procedure My_Command_Received (Connection : Connection_Type;
                                   Update : Update_Type) is
        Json_Doc : Json_Document;

    begin
        Put_Line ("Command received...");
        Put (Update);
        
        if Update.Message.Text = Ww2us ("/start") then
            Send_Message (Connection,
                          Update.Message.Chat.Id,
                          Ww2us ("Inline keyboard example:"),
                          Reply_Markup => Inline_Keyboard,
                          Json => Json_Doc);
        else
            Reply_Message (Connection,
                           Update,
                           Ww2us ("Command unknown. Try /start."));
        end if;
    end My_Command_Received;
    
    Token_Data : Token_Type;
    Connection : Connection_Type;
        
begin
    Get_Token (Token_Data);
    Connection.Token := Token_Data;
    
    Build_Keyboard;
    
    declare 
        package My_Bot is new Telegram.Bots 
          (
           Connection => Connection,
           Update_Received => My_Update_Received,
           Command_Received => My_Command_Received
          );
    begin
        Put_Line ("Inline keyboard bot started");
        My_Bot.Start_Loop;
    end;
end Inline_Keyboard;
