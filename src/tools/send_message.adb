-- send_message.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Wide_Wide_Text_Io;
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Characters.Conversions;

with League.Strings;
use League.Strings;
with League.Json.Documents;
use League.Json.Documents;
--  with League.Json.Objects;
--  use League.Json.Objects;
--  with League.Json.Values;
--  use League.Json.Values;

with Telegram.Data;
use Telegram.Data;
with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Puts;
use Telegram.Puts;

with Get_Token;

procedure Send_Message is
    
    package Chacon renames Ada.Characters.Conversions;
    package Wwio renames Ada.Wide_Wide_Text_Io;
    
    function "+"(Item : String) return Universal_String is
        Wwstring : Wide_Wide_String := Chacon.To_Wide_Wide_String(Item);
    begin
        return To_Universal_String(Wwstring);
    end "+";
        
    Token_Data : Token_Type;
    Connection : Connection_Type;
    
    Chat_Id : Long_Long_Integer;
    Text : Universal_String;
    
    Json : Json_Document;
    Json_Str : Universal_String;
begin 
    if Argument_Count < 2 then
        Put_Line ("Synopsis:");
        Put_Line ("    bin/send_message CHAT_ID TEXT");
    end if;
    
    Chat_Id := Long_Long_Integer'Value (Argument (1));
    Text := To_Universal_String ( Chacon.To_Wide_Wide_String (Argument (2)));
    
    Get_Token (Token_Data);
    Connection.Token := Token_Data;
    
    --  Put_Line ("Token: ");
    --  Put_Line (String (Token_Data));
    
    Put_Line ("URL : " & Get_Url (Connection, "sendMessage"));
    Put_Line ("Chat id:" & Chat_Id'Image);
    Put ("Text: ");
    Wwio.Put_Line (To_Wide_Wide_String (Text));
    
    Put_Line ("Sending message...");
    Send_Message (Connection, Chat_Id, Text, 
                  Json => Json);
        
    Put_Line ("Done.");
    
    Put_Line ("Answer:");
    Json_Str := Json.To_Json;
    Wwio.Put_Line (To_Wide_Wide_String (Json_Str));
    
end Send_Message;
