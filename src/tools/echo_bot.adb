--  echo_bot.adb ---

--  Copyright 2020 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
use Ada.Text_Io;
--  with Ada.Wide_Wide_Text_Io;

with League.Json.Documents;
use League.Json.Documents;
with League.Strings;
use League.Strings;

with Telegram.Data;
use Telegram.Data;
with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Bots;
--  no use telegram.bots needed
with Telegram.Puts;
use Telegram.Puts;

with Get_Token;

procedure Echo_Bot is
    
    function Ww2us (S : Wide_Wide_String) return Universal_String 
      renames League.Strings.To_Universal_String;
    
    procedure My_Update_Received (Connection : Connection_Type;
                                  Update : Update_Type) is
        Json_Doc : Json_Document;
    begin
        Put_Line ("Update received...");
        Put (Update);
        
        Send_Message (Connection, 
                      Update.Message.Chat.Id,
                      Update.Message.Text,
                      Json => Json_Doc);
    end My_Update_Received;
    
    procedure My_Command_Received (Connection : Connection_Type;
                                   Update : Update_Type) is
        Json_Doc : Json_Document;
        
        Start_Message : Wide_Wide_String := 
          "A bot that repeats your message back";
        Help_Message : Wide_Wide_String := 
          "/help This message. /start Introduction message." & 
          "Send a message to the bot.";
    begin
        Put_Line ("Command received...");
        Put (Update);
        
        if Update.Message.Text = Ww2us ("/help") then
            Reply_Message (Connection,
                           Update,
                           Ww2us (Help_Message));
        elsif Update.Message.Text = Ww2us ("/start") then
            Reply_Message (Connection,
                          Update,
                          Ww2us (Start_Message));
        else
            Reply_Message (Connection,
                           Update,
                           Ww2us ("Command unknown. Try /start or /help."));
        end if;
    end My_Command_Received;
    
    Token_Data : Token_Type;
    Connection : Connection_Type;
        
begin
    Get_Token (Token_Data);
    Connection.Token := Token_Data;
    
    declare 
        package My_Bot is new Telegram.Bots 
          (
           Connection => Connection,
           Update_Received => My_Update_Received,
           Command_Received => My_Command_Received
          );
    begin
        Put_Line ("Echo bot started");
        My_Bot.Start_Loop;
    end;
end Echo_Bot;
