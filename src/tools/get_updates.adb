-- get_updates.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------


with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_Io;
use Ada.Text_Io;

with League.Json.Documents;
use League.Json.Documents;

with Telegram.Data;
use Telegram.Data;
with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Puts;
use Telegram.Puts;

with Get_Token;

procedure Get_Updates is
    
    Connection : Connection_Type;    
    Token_Data : Token_Type;
    
    Jdoc : Json_Document;
    Updates : Update_Vectors.Vector;
    
    procedure Print_Updates is
        use Update_Vectors;
        Length : Integer := Integer (Updates.Length);
    begin
        
        for I in 1 .. Length  loop
            Put_Line ("---- ----");
            Put_Line ("Update number " & I'Image);
            Put_Line ("---- ----");
            
            Put (Updates.Element (I));      
        end loop;
    end Print_Updates;
    
begin
    if Argument_Count > 0 then
        Put_Line ("Synopsis:");
        Put_Line ("    bin/get_updates");
    end if;
    
    Get_Token (Token_Data);
    Connection.Token := Token_Data;
    
    --  Put_Line ("Token: ");
    --  Put_Line (String (Token_Data));
    
    Put_Line ("Calling getUpdate method...");
    Get_Updates (Connection => Connection, 
                 Json => Jdoc);
    Put_Line ("Updates received.");
    
    Updates := Update_Array_From_Json (Jdoc);
    
    if Updates.Is_Empty then
        Put_Line ("No Update founded on server.");
    else
        Print_Updates;
    end if;
end Get_Updates;
