-- get_me.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Wide_Wide_Text_Io;
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Characters.Conversions;

with League.Strings;
use League.Strings;
with League.Json.Documents;
use League.Json.Documents;
with League.Json.Objects;
use League.Json.Objects;
with League.Json.Values;
use League.Json.Values;

with Telegram.Data;
use Telegram.Data;
with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Puts;
use Telegram.Puts;

with Get_Token;

procedure Get_Me is
    
    package Chacon renames Ada.Characters.Conversions;
    package Wwio renames Ada.Wide_Wide_Text_Io;
    
    function "+"(Item : String) return Universal_String is
        Wwstring : Wide_Wide_String := Chacon.To_Wide_Wide_String(Item);
    begin
        return To_Universal_String(Wwstring);
    end "+";
        
    Token_Data : Token_Type;
    Connection : Connection_Type;
    
    Json : Json_Document;
    
    User : User_Type;
begin 
    Get_Token (Token_Data);
    Connection.Token := Token_Data;
    
    Put_Line ("Token: ");
    Put_Line (String (Token_Data));
    
    Put_Line ("URL : " & Get_Url (Connection, "getMe"));
    
    Put_Line ("Sending getMe petition...");
    Get_Me (Connection, Json);
    Put_Line ("Done. Parsing...");
    
    User := From_Json (Json);
    Put (User);
    
end Get_Me;
