with Ada.Text_Io;
use Ada.Text_Io;

procedure Get_Token (Token : out Token_Type) is
    File : File_Type;
begin
    Open (File, In_File, "config/token.conf");
    
    declare
        Line : String := Get_Line (File);
    begin
        Token := Token_Type (Line);
    end;
    
    Close (File);
end Get_Token;
