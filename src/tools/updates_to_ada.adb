--  updates_to_ada.adb ---

--  Copyright 2019 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Command_Line;
use Ada.Command_Line;
with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Wide_Wide_Text_Io;
with Ada.Characters.Conversions;
use Ada.Characters.Conversions;

with League.Strings;
use League.Strings;
with League.Json.Documents;
use League.Json.Documents;

with Telegram.Data;
use Telegram.Data;
with Telegram.Puts;
use Telegram.Puts;

procedure Updates_To_Ada is
    
    package Wwio renames Ada.Wide_Wide_Text_Io;
    
    procedure Read_File (Filename : String; Data : out Universal_String) is
        File : File_Type;
    begin
        Open (File, In_File, Filename);
        
        while (not End_Of_File (File)) loop
            declare 
                Line : String := Get_Line (File);
            begin
                Data.Append (To_Wide_Wide_String (Line));
            end;
        end loop;
        
        Close (File);        
    end Read_File;
    
    Json_Str : Universal_String;
    Jdoc : Json_Document;
    Updates : Update_Vectors.Vector;
begin
    if Argument_Count < 1 then
        Put_Line ("Synopsis:");
        Put_Line ("    bin/updates_to_ada JSON_FILE");
    end if;
    
    Read_File (Argument (1), Json_Str);
    Put_Line ("File red:");
    Wwio.Put_Line (To_Wide_Wide_String (Json_Str));
    
    Jdoc := From_Json (Json_Str);
    
    Updates := Update_Array_From_Json (Jdoc);
    
    Put (Updates.Element (1));
end Updates_To_Ada;
