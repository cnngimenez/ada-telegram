-- telegram-data.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings.Fixed;
with Ada.Strings;
with Ada.Characters.Conversions;

with League.Json.Arrays;
use League.Json.Arrays;
with League.Json.Values;
use League.Json.Values;

package body Telegram.Data is
    package Chacon renames Ada.Characters.Conversions;
    
    function "+"(Item : String) return Universal_String is
        Wwstring : Wide_Wide_String := Chacon.To_Wide_Wide_String(Item);
    begin
        return To_Universal_String(Wwstring);
    end "+";

    
    function User_From_Json (Jobj : Json_Object) return User_Type is
        User : User_Type;
        
        Jresults : Json_Object;
    begin
        --  if it is encapsulated inside a result HTTP answer use the 
        --  "result" field. If not try to process the entire JSON.
        if Jobj.Contains (+"result") then
            Jresults := Jobj.Value (+"result").To_Object;
        else
            Jresults := Jobj;
        end if;
        
        declare
            Jid : Json_Value := Jresults.Value (+"id");
            Jis_Bot : Json_Value := Jresults.Value (+"is_bot");
            Jfirst_Name : Json_Value := Jresults.Value (+"first_name");
            Jlast_Name : Json_Value := Jresults.Value (+"last_name");
            Jusername : Json_Value := Jresults.Value (+"username");
            Jlang_Code : Json_Value := Jresults.Value (+"language_code");
            Jcan_Join_Groups : Json_Value := Jresults.Value (+"can_join_groups");
            Jcan_Read_All_Group_Messages : Json_Value := 
              Jresults.Value (+"can_read_all_group_messages");
            Jsupport_Inline_Queries : Json_Value := 
              Jresults.Value (+"supports_inline_queries");
        begin
            User.Id := Long_Long_Integer (Jid.To_Integer);
            User.Is_Bot := Jis_Bot.To_Boolean;
            User.First_Name := Jfirst_Name.To_String;
            User.Last_Name := Jlast_Name.To_String;
            User.Username := Jusername.To_String;
            User.Language_Code := Jlang_Code.To_String;
            User.Can_Join_Groups := Jcan_Join_Groups.To_Boolean;
            User.Can_Read_All_Group_Messages := 
              Jcan_Read_All_Group_Messages.To_Boolean;
            User.Supports_Inline_Queries := Jsupport_Inline_Queries.To_Boolean;
            
        end;

        return User;
    end User_From_Json;
    
    function User_From_Json (Jdoc : Json_Document) return User_Type is
    begin
        return User_From_Json (Jdoc.To_Json_Object);
    end User_From_Json;
    
    function Chat_From_Json (Jobj : Json_Object) return Chat_Type is
        Chat : Chat_Type;
        Jresults : Json_Object;
    begin
        Chat.Pinned_Message := new Message_Type;
        
        if Jobj.Contains (+"result") then
            Jresults := Jobj.Value (+"result").To_Object;
        else
            Jresults := Jobj;
        end if;
        
        declare
            Jid : Json_Value := Jresults.Value (+"id");
            Jtype : Json_Value := Jresults.Value (+"type");
            Jtitle : Json_Value := Jresults.Value (+"title");
            Jusername : Json_Value := Jresults.Value (+"username");
            Jfirst_Name : Json_Value := Jresults.Value (+"first_name");
            Jlast_Name : Json_Value := Jresults.Value (+"last_name");
            --  Photo : ChatPhoto;
            Jdescription : Json_Value := Jresults.Value (+"description");
            Jinvite_Link : Json_Value := Jresults.Value (+"invite_link");
            Jpinned_Message : Json_Value := Jresults.Value (+"pinned_message");
            --  Permissions : ChatPermissions;
            Jslow_Mode_Delay : Json_Value := 
              Jresults.Value (+"slow_mode_delay");
            Jsticker_Set_Name : Json_Value := 
              Jresults.Value (+"sticker_set_name");
            Jcan_Set_Sticker_Set : Json_Value := 
              Jresults.Value (+"can_set_sticker_set");
            Message : Message_Type;
        begin
            Chat.Id :=  Long_Long_Integer (Jid.To_Integer);
            Chat.Chat_type := Jtype.To_String; --  "type" field
            Chat.Title := Jtitle.To_String;
            Chat.Username := Jusername.To_String;
            Chat.First_Name := Jfirst_Name.To_String;
            Chat.Last_Name := Jlast_Name.To_String;
            --  Photo : ChatPhoto;
            Chat.Description := Jdescription.To_String;
            Chat.Invite_Link := Jinvite_Link.To_String;
            -- Chat.Pinned_Message := new Message_Type;
            if Jobj.Contains (+"pinned_message") then
                Message := Message_From_Json (Jpinned_Message.To_Object);
                Chat.Pinned_Message.all := Message;
            else 
                Chat.Pinned_Message := new Message_Type'(Invalid => True);
            end if;

            --  Permissions : ChatPermissions;
            Chat.Slow_Mode_Delay := 
              Integer (Jslow_Mode_Delay.To_Integer);
            Chat.Sticker_Set_Name := Jsticker_Set_Name.To_String;
            Chat.Can_Set_Sticker_Set := Jcan_Set_Sticker_Set.To_Boolean;
        end;
        
        return Chat;
    end Chat_From_Json;
    
    function Chat_From_Json (Jdoc : Json_Document) return Chat_Type is
    begin
        return Chat_From_Json (Jdoc.To_Json_Object);
    end Chat_From_Json;

    function Message_From_Json (Jobj : Json_Object) return Message_Type is
        Message : Message_Type;
        
        Jresults : Json_Object;
    begin
        if Jobj.Contains (+"result") then
            Jresults := Jobj.Value (+"result").To_Object;
        else
            Jresults := Jobj;
        end if;
        
        declare
            Jmessage_Id : Json_Value := Jresults.Value (+"message_id");
            Jfrom : Json_Value := Jresults.Value (+"from");
            Jdate : Json_Value := Jresults.Value (+"date");
            Jchat : Json_Value := Jresults.Value (+"chat");
            Jtext : Json_Value := Jresults.Value (+"text");
        begin
            Message.Message_Id := Long_Long_Integer (Jmessage_Id.To_Integer);
            if Jfrom.Is_Empty then
                Message.From := Invalid_User;
            else
                Message.From := User_From_Json (Jfrom.To_Object);
            end if;
            Message.Date := Integer (Jdate.To_Integer);
            if Jchat.Is_Empty then
                Message.Chat := Invalid_Chat;
            else
                Message.Chat := Chat_From_Json (Jchat.To_Object);
            end if;
            Message.Text := Jtext.To_String;
        end;
        
        return Message;
    end Message_From_Json;
    
    function Message_From_Json (Jdoc : Json_Document) return Message_Type is
    begin
        return Message_From_Json (Jdoc.To_Json_Object);
    end Message_From_Json;
    
    function Update_From_Json (Jobj : Json_Object) return Update_Type is
        Update : Update_Type;
        
        Jresults : Json_Object;
    begin
        if Jobj.Contains (+"result") then
            Jresults := Jobj.Value (+"result").To_Object;
        else
            Jresults := Jobj;
        end if;
        
        declare
            Jupdate_Id : Json_Value := Jresults.Value (+"update_id");
            Jmessage : Json_Value := Jresults.Value (+"message");
            Jedited_Message : Json_Value := Jresults.Value (+"edited_message");
            Jchannel_Post : Json_Value := Jresults.Value (+"channel_post");
            Jedited_Channel_Post : Json_Value := 
              Jresults.Value (+"edited_channel_post");
            --  Inline_Query : Inline_Query_Type;
            --  Chosen_Inline_Result : Chosen_Inline_Result_Type;
            --  Callback_Query : Callback_Query_Type;
            --  Shipping_Query : Shipping_Query_Type;
            --  Pre_Checkout_Query : Pre_Checkout_Query_Type;
            --  Poll : Poll_Type;
            --  Poll_Answer : Poll_Answer_Type;
        begin
            Update.Update_Id := 
              Long_Long_Integer (Jupdate_Id.To_Integer);
            
            if Jmessage.Is_Empty then
                Update.Message := Invalid_Message;
            else
                Update.Message := 
                  Message_From_Json (Jmessage.To_Object);
            end if;
            
            if Jedited_Message.Is_Empty then
                Update.Edited_Message := Invalid_Message;
            else
                Update.Edited_Message := 
                  Message_From_Json (Jedited_Message.To_Object);
            end if;
            
            if Jchannel_Post.Is_Empty then
                Update.Channel_Post := Invalid_Message;
            else
                Update.Channel_Post := From_Json (Jchannel_Post.To_Object);
            end if;
            
            if Jedited_Channel_Post.Is_Empty then
                Update.Edited_Channel_Post := Invalid_Message;
            else
                Update.Edited_Channel_Post := 
                  From_Json (Jedited_Channel_Post.To_Object);
            end if;
                
            --  Inline_Query : Inline_Query_Type;
            --  Chosen_Inline_Result : Chosen_Inline_Result_Type;
            --  Callback_Query : Callback_Query_Type;
            --  Shipping_Query : Shipping_Query_Type;
            --  Pre_Checkout_Query : Pre_Checkout_Query_Type;
            --  Poll : Poll_Type;
            --  Poll_Answer : Poll_Answer_Type;            
        end;
        
        return Update;
    end Update_From_Json;
    
    function Update_From_Json (Jdoc : Json_Document) return Update_Type is
    begin
        return Update_From_Json (Jdoc.To_Json_Object);
    end Update_From_Json;
    
    
    function Update_Array_From_Json (Jarray : Json_Array) 
                                    return Update_Vectors.Vector is
        Amount : Natural := Jarray.Length;
        Jobj : Json_Object;
        
        Updates : Update_Vectors.Vector;
    begin
        for I in 1..Amount loop
            Jobj := Jarray.Element (I).To_Object;
            Update_Vectors.Append (Updates, Update_From_Json (Jobj));
        end loop;

        return Updates;
    end Update_Array_From_Json;
    
    function Update_Array_From_Json (Jobj : Json_Object) 
                                    return Update_Vectors.Vector is
        Updates : Update_Vectors.Vector;
        
        Jresults : Json_Array;
    begin
        if Jobj.Contains (+"result") then
            Jresults := Jobj.Value (+"result").To_Array;
            return Update_Array_From_Json (Jresults);
        else 
            return Updates;
        end if;
    end Update_Array_From_Json;
    
    function Update_Array_From_Json (Jdoc : Json_Document) 
                                    return Update_Vectors.Vector is
    begin
        return Update_Array_From_Json (Jdoc.To_Json_Object);
    end Update_Array_From_Json;
    
    function Epoch_Seconds_To_Time (Date : Integer) return Time is
        use Ada.Calendar;
        
        Epoch_Time : constant Time := Time_Of (1970, 1 ,1);
    begin
        return Epoch_Time + Duration (Date);
    end Epoch_Seconds_To_Time;
    
    function Epoch_Seconds_To_String (Seconds : Integer) 
                                     return Universal_String is
        function Us (Str : String) return Universal_String is
            use Ada.Characters;
        begin
            return To_Universal_String 
              ( Conversions.To_Wide_Wide_String (Str) );
        end Us;
        
        T : Time;
        Str : Universal_String;
        
        Sec : Duration;
        Minh, Sech, Hours, Mins : Natural;
        
        use Ada.Strings.Fixed;
        use Ada.Strings;
    begin
        T := Epoch_Seconds_To_Time (Seconds);
        
        Sec := Ada.Calendar.Seconds (T);
        
        Mins := Natural (Sec) / 60;
        Hours := Mins / 60;
        
        Sech := Natural (Sec) - Mins * 60;
        Minh := Mins - Hours * 60;
        
        Str := Us(Trim(Year(T)'Image, Left)) & Us("-")
          & Us(Trim(Month(T)'Image, Left)) & Us("-")
          & Us(Trim(Day(T)'Image, Left)) & Us(" ")
          & Us(Trim(Hours'Image, Left)) & Us(":") 
          & Us(Trim(Minh'Image, Left)) & Us(":")
          & Us(Trim(Sech'Image, Left)) & Us(":");
                
        return Str;
    end Epoch_Seconds_To_String;
    
    function Inline_Button_To_Json_Value (Button : Inline_Button_Type)
                                         return Json_Value is 
        Jobj : Json_Object;
    begin
        Jobj.Insert (+"text", 
                     To_Json_Value (Button.Text));
        
        if Button.Url /= Empty_Universal_String then
            Jobj.Insert (+"url",
                         To_Json_Value (Button.Url));
        end if;
        
        if Button.Callback_Data /= Empty_Universal_String then
            Jobj.Insert (+"callback_data",
                         To_Json_Value (Button.Callback_Data));
        end if;
        
        return Jobj.To_Json_Value;
    end Inline_Button_To_Json_Value;
    
    function Inline_Keyboard_To_Json_Array
      (Keyboard : Inline_Keyboard_Vectors.Vector) return Json_Array is
        Length : Integer := Integer (Keyboard.Length);
        
        Jkeyboard : Json_Array;
    begin
        
        for I in 1 .. Length loop
            declare 
                Jrow : Json_Array;
                Button : Inline_Button_Type := Keyboard (I);
            begin
                Jrow.Append 
                  (Inline_Button_To_Json_Value (Button));
                
                Jkeyboard.Append (Jrow.To_Json_Value);
            end;
        end loop;
        
        return Jkeyboard;        
    end Inline_Keyboard_To_Json_Array;
    
end Telegram.Data;
