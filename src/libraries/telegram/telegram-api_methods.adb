-- telegram-api_methods.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;
with Ada.Characters.Conversions;
use Ada.Characters.Conversions;
with Ada.Strings.Unbounded;


--  Uncomment for debugging purposes:
--  with Ada.Wide_Wide_Text_Io;
--  with Ada.Text_Io;
--  use Ada.Text_Io;

with Util.Http.Clients;
use Util.Http.Clients;
with Util.Http.Clients.Curl;

with League.Holders;
with League.Holders.Long_Long_Integers;
with League.Json.Values;
use League.Json.Values;
with League.Json.Objects;
use League.Json.Objects;
with League.Json.Arrays;
use League.Json.Arrays;
with League.Characters;
with League.Strings;
use League.Strings;

package body Telegram.Api_Methods is
    
    --  package Wwio renames Ada.Wide_Wide_Text_Io;
    
    function S2ww (S : String) return Wide_Wide_String 
      renames Ada.Characters.Conversions.To_Wide_Wide_String;
    
    function S2us (S : String) return Universal_String is
    begin
        return To_Universal_String (S2ww (S));
    end S2us;
    
    --  Convert from Wide_Wide_String to String
    --
    --  WARNING: Information loss. All Wide_Wide_Character types greater that
    --  256 (8-bits) will be ignored.
    function Wws2s (Wws : Wide_Wide_String) return String is
        use Ada.Strings.Unbounded;
        
        Amount : Natural := Wws'Length;
        Temp_Str : Unbounded_String;
        Wwc : Wide_Wide_Character;
        C : Character;
    begin
        for I in Natural range 1 .. Amount loop
            Wwc := Wws (I);
            if (Wide_Wide_Character'Pos (Wwc) < 256) then 
                C := Character'Val (Wide_Wide_Character'Pos (Wwc));
                Append (Temp_Str, C);
            end if;
        end loop;
        
        return To_String (Temp_Str);
    end Wws2s;
    
    Url_Prefix : constant String := "https://api.telegram.org/bot";
    
    function Get_Url (Connection : Connection_Type;
                      Method_Name : String;
                      Parameters : String := "") return String is
        Url : String := Url_Prefix & String (Connection.Token) & "/";    
        Url_Base : String := Url & Method_Name;
    begin 
        
        if Parameters /= "" then
            return Url_Base  & "?" & Parameters;
        else
            return Url_Base;
        end if;

    end Get_Url;
    
    --  Do an HTTP GET to the API of Telegram. 
    --  Parameters are sent through the URL.
    --
    --  Returns the HTTP body response getted from the server.
    procedure Api_Get (Connection : Connection_Type;
                       Method_Name : String;                     
                       Output : out Universal_String;
                       Parameters : String := ""
                      ) 
    is 
        Url : String := Get_Url (Connection, Method_Name, Parameters);
        
        Http_Client : Client;
        Http_Response : Response;
    begin
        --  For debugging purposes:
        -- Put ("-- Api_Get:");
        -- Put_Line (Url);
        
        Http_Client.Get (Url, Http_Response);
        
        Output := S2us (Http_Response.Get_Body);
    end Api_Get;
    
    --  Do an HTTP POST to the Telegram API. 
    --  Parameters are sent on the body of the request.
    --
    --  Return the HTTP body response getted from the server.
    procedure Api_Post (Connection : Connection_Type;
                        Method_Name : String;
                        Data : String := "";
                        Output : out Universal_String
                       )
    is
        Url : String := Get_Url (Connection, Method_Name);
        
        Http_Client : Client;
        Http_Response : Response;
    begin
        Http_Client.Set_Header ("content-type", "application/json");
        Http_Client.Post (Url, Data, Http_Response);
        
        --  for debugging purpose:
        --  Put_Line ("-- Api_Post");
        --  Put_Line (Url);
        --  Put_Line (Data);
        
        Output := S2us (Http_Response.Get_Body);        
        
        --  Wwio.Put_Line (To_Wide_Wide_String (Output));
    end Api_Post;
    
    procedure Api_Post (Connection : Connection_Type;
                        Method_Name : String;
                        Data : Universal_String;
                        Output : out Universal_String)
    is 
        Data_Str : String := Wws2s (To_Wide_Wide_String (Data));
    begin
        Api_Post (Connection, Method_Name, Data_Str, Output);
    end Api_Post;
    
    
    procedure Get_Me (Connection : Connection_Type;
                      Json : out Json_Document) is
        Output : Universal_String;
    begin
        Api_Get (Connection, "getMe", Output);
        
        Json := From_Json (Output);
    end Get_Me;
    
    procedure Get_Updates (Connection : Connection_Type;
                           Offset : Long_Long_Integer := 0;
                           Limit : Integer := -1;
                           Timeout : Integer := 0;
                             -- Allowed_Updates : array <> of String) 
                           Json : out Json_Document
                          )
    is
        function Process_Parameters (Offset : Long_Long_Integer; 
                                     Limit : Integer;
                                     Timeout : Integer) 
                                    return String 
        is
            use Ada.Strings.Unbounded;
            
            Str : Unbounded_String;
        begin
            if Offset /= 0 then
                Append (Str, "offset=");
                Append (Str, Trim(Offset'Image, Both));
            end if;
            
            if Limit /= -1 then
                if Length (Str) /= 0 then
                    Append (Str, "&");
                end if;
                
                Append (Str,"limit=");
                Append (Str, Trim(Limit'Image, Both));
            end if;
            
            if Timeout /= 0 then
                if Length (Str) /= 0 then
                    Append (Str, "&");
                end if;
                
                Append (Str, "timeout=");
                Append (Str, Trim(Timeout'Image, Both));
            end if;
            
            return To_String (Str);
        end Process_Parameters;
        
        Output : Universal_String;
        Parameters : String := Process_Parameters (Offset, Limit, Timeout);
    begin
        Api_Get (Connection, "getUpdates", Output, Parameters);
        
        Json := From_Json (Output);
    end Get_Updates;
    
    --  Convert a Parse_Mode_Type to its Universal_String representation.
    function Parse_Mode_To_Us (Parse_Mode : Parse_Mode_Type) 
                              return Universal_String is
    begin
        case Parse_Mode is
           when Html =>
               return S2us ("HTML");
           when Markdown =>
               return S2us ("Markdown");
           when others =>
            return Empty_Universal_String;
        end case;
    end Parse_Mode_To_Us;
    
    --  Process these procedure parameters and create the JSON data
    --  as string.
    function Generate_Data 
      (Chat_Id : Long_Long_Integer;
       Text : Universal_String;
       Parse_Mode : Parse_Mode_Type := Undefined;
       Disable_Web_Page_Preview : Boolean := False;
       Disable_Notification : Boolean := False;
       Reply_To_Message_Id : Long_Long_Integer := 0
      ) return Json_Object is
        
        use League.Holders.Long_Long_Integers;
        use League.Holders;
        
        Jobj : Json_Object;
        
    begin
        Jobj.Insert (S2us ("chat_id"), 
                     To_Json_Value (To_Holder (Chat_Id)));
        Jobj.Insert (S2us ("text"), 
                     To_Json_Value (Text));
        
        if Parse_Mode /= Undefined then
            Jobj.Insert (S2us ("parse_mode"), 
                         To_Json_Value 
                           (To_Holder 
                              (Parse_Mode_To_Us (Parse_Mode))));
        end if;
        
        if Disable_Web_Page_Preview then
            Jobj.Insert (S2us ("disable_web_page_preview"),
                         To_Json_Value (True));
        end if;
        
        if Disable_Notification then
            Jobj.Insert (S2us ("disable_notification"),
                         To_Json_Value (True));
        end if;
        
        if Reply_To_Message_Id /= 0 then
            Jobj.Insert (S2us ("reply_to_message_id"),
                         To_Json_Value (To_Holder (Reply_To_Message_Id)));
        end if;
        
        return Jobj;
    end Generate_Data;
    
    function Generate_Inline_Markup 
      (Reply_Markup : Inline_Keyboard_Vectors.Vector) 
      return Json_Object is
        JArray : Json_Array;
        Jobj : Json_Object;
    begin
        Jarray := Inline_Keyboard_To_Json_Array (Reply_Markup);
        
        Jobj.Insert (S2us ("inline_keyboard"),
                     Jarray.To_Json_Value);
        
        return Jobj;
    end Generate_Inline_Markup;
    
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0;
                            -- Reply_Markup :
                            Json : out Json_Document) is
                
        Output : Universal_String;

        jdata : Json_Object;
        
    begin        
        JData := Generate_Data
          (Chat_Id, Text, Parse_Mode, Disable_Web_Page_Preview,
           Disable_Notification, Reply_To_Message_Id);
        
        Api_Post (Connection, "sendMessage", 
                  jData.To_Json_Document.To_Json,
                  Output);
        
        Json := From_Json (Output);
    end Send_Message;
    
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0;
                            Reply_Markup : Inline_Keyboard_Vectors.Vector;
                            Json : out Json_Document) is
        
        Output : Universal_String;
        
        Jdata : Json_Object := Generate_Data
          (Chat_Id, Text, Parse_Mode, Disable_Web_Page_Preview,
           Disable_Notification, Reply_To_Message_Id);
        
    begin
        JData.Insert 
          (S2us("reply_markup"), 
           To_Json_Value(Generate_Inline_Markup (Reply_Markup)));
        Api_Post (Connection, "sendMessage", 
                  JData.To_Json_Document.To_Json,
                  Output);
        
        Json := From_Json (Output);
    end Send_Message;

    
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0
                           ) is
        Json_Doc : Json_Document;
    begin
        Send_Message (Connection, Chat_Id, Text,
                      Parse_Mode, Disable_Web_Page_Preview,
                      Disable_Notification, Reply_To_Message_Id,
                      Json_Doc);
    end Send_Message;
    
    procedure Reply_Message (Connection : Connection_Type;
                             Chat_Id : Long_Long_Integer;
                             Reply_To_Message_Id : Long_Long_Integer;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                            ) is
    begin
        Send_Message (Connection, Chat_Id, Text,
                      Parse_Mode, Disable_Web_Page_Preview,
                      Disable_Notification, Reply_To_Message_Id);
    end Reply_Message;
    
    procedure Reply_Message (Connection : Connection_Type;
                             Message : Message_Type;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                            ) is
    begin
        Reply_Message (Connection, Message.Chat.Id, Message.Message_Id, Text,
                       Parse_Mode, Disable_Web_Page_Preview,
                       Disable_Notification);
    end Reply_Message;
    
    procedure Reply_Message (Connection : Connection_Type;
                             Update : Update_Type;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                            ) is
    begin
        Reply_Message (Connection, Update.Message.Chat.Id, 
                       Update.Message.Message_Id, Text,
                       Parse_Mode, Disable_Web_Page_Preview,
                       Disable_Notification);
    end Reply_Message;

    
begin
    Curl.Register;
    
end Telegram.Api_Methods;
