-- telegram-api_methods.ads --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with League.Strings;
use League.Strings;
with League.Json.Documents;
use League.Json.Documents;

with Telegram.Data;
use Telegram.Data;

package Telegram.Api_Methods is
    
    type Token_Type is new String (1 .. 45);
    
    type Connection_Type is tagged record
        Token : Token_Type;
    end record;
    
    function Get_Url (Connection : Connection_Type;
                      Method_Name : String;
                      Parameters : String := "") 
                     return String;
    
    procedure Get_Me (Connection : Connection_Type;
                      Json : out Json_Document);
    
    procedure Get_Updates (Connection : Connection_Type;
                           Offset : Long_Long_Integer := 0;
                           Limit : Integer := -1;
                           Timeout : Integer := 0;
                             -- Allowed_Updates : array <> of String);
                           Json : out Json_Document
                          );
    
    type Parse_Mode_Type is (Undefined, Html, Markdown);
    
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0;
                            Json : out Json_Document);
    
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0;
                            Reply_Markup : Inline_Keyboard_Vectors.Vector;
                            Json : out Json_Document);
    
    --  procedure Send_Message (Connection : Connection_Type;
    --                          Chat_Id : Long_Long_Integer;
    --                          Text : Universal_String;
    --                          Parse_Mode : Parse_Mode_Type := Undefined;
    --                          Disable_Web_Page_Preview : Boolean := False;
    --                          Disable_Notification : Boolean := False;
    --                          Reply_To_Message_Id : Long_Long_Integer := 0;
    --                          Reply_Markup : Reply_Keyboard_Vectors.Vector :=
    --                            Invalid_Reply_Keyboard;
    --                          Json : out Json_Document);
        
    
    --  Sometimes the output is not needed.
    procedure Send_Message (Connection : Connection_Type;
                            Chat_Id : Long_Long_Integer;
                            Text : Universal_String;
                            Parse_Mode : Parse_Mode_Type := Undefined;
                            Disable_Web_Page_Preview : Boolean := False;
                            Disable_Notification : Boolean := False;
                            Reply_To_Message_Id : Long_Long_Integer := 0
                           );
    
    --  Convenience procedures.
    procedure Reply_Message (Connection : Connection_Type;
                             Chat_Id : Long_Long_Integer;
                             Reply_To_Message_Id : Long_Long_Integer;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                            );
    procedure Reply_Message (Connection : Connection_Type;
                             Message : Message_Type;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                            );
    procedure Reply_Message (Connection : Connection_Type;
                             Update : Update_Type;
                             Text : Universal_String;
                             Parse_Mode : Parse_Mode_Type := Undefined;
                             Disable_Web_Page_Preview : Boolean := False;
                             Disable_Notification : Boolean := False
                           );
end Telegram.Api_Methods;
