-- telegram-puts.adb --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Text_Io;
use Ada.Text_Io;
with Ada.Wide_Wide_Text_Io;
with Ada.Characters.Conversions;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;

with League.Strings;
use League.Strings;

with Emojis;
use Emojis;
with Console;
use Console;

package body Telegram.Puts is
    
    package Wwio renames Ada.Wide_Wide_Text_Io;
    
    function S2wws (Item : String) return Wide_Wide_String renames
      Ada.Characters.Conversions.To_Wide_Wide_String;
    function Us2wws (Item : Universal_String'Class)
                    return Wide_Wide_String renames
      League.Strings.To_Wide_Wide_String;
    
    --  Print the user type into standard output.
    procedure Put (User : User_Type; Tab : Natural := 0) is
        Prefix : String := Tab * " ";
    begin
        if User.Invalid then
            Set_Colour (Red);
            Put_Line (Prefix & "User data is empty or invalid");
            Reset_All;
        else
            Bold;
            Put (Prefix & "**** User ");
            if User.Is_Bot then
                Put_Emoji (16#1F916#);
            else
                Put_Skin (Wide_Wide_Character'Val(16#1F9D1#), Medium_Dark);
                Put_Skin (Wide_Wide_Character'Val(16#1F469#), Medium_Dark);
            end if;
            Put_Line (":");
            Reset_All;
            
            Put_Line (Prefix & "Id: " & User.Id'Image);
            
            Put_Line (Prefix & "Is bot: " & User.Is_Bot'Image);
            
            Put (Prefix);
            Wwio.Put_Line ("First name: " & Us2wws (User.First_Name));
            
            Put (Prefix);
            Wwio.Put_Line ("Last name: " & Us2wws (User.Last_Name));
            
            Put (Prefix);
            Wwio.Put_Line ("Username: " & Us2wws (User.Username));
            
            Put (Prefix);
            Wwio.Put_Line ("Language code: " & Us2wws (User.Language_Code));
            
            Put_Line (Prefix & "Can join groups: " & 
                        User.Can_Join_Groups'Image);
            
            Put_Line (Prefix & "Can read all group messages: " & 
                        User.Can_Read_All_Group_Messages'Image);
            
            Put_Line (Prefix & "Supports inline queries: " & 
                        User.Supports_Inline_Queries'Image);
            
            Put_Line (Prefix & "-- End User data");
        end if;
    end Put;
    
    procedure Put (Update : Update_Type; Tab : Natural := 0) is
        Prefix : String := Tab * " ";        
    begin
        if Update.Invalid then
            Set_Colour (Red);
            Put_Line (Prefix & "Update is empty or invalid");
            Reset_All;
        else
            Bold;
            Put_Line (Prefix & "**** Update:");
            Reset_All;
            
            Put_Line (Prefix & "Update Id: " & Update.Update_Id'Image);
            
            Put_Line (Prefix & "Message: ");
            Put (Update.Message, Tab + 2);
            
            Put_Line (Prefix & "Edited message: ");
            Put (Update.Edited_Message, Tab + 2);
            
            Put_Line (Prefix & "Channel post: ");
            Put (Update.Channel_Post, Tab + 2);
            
            Put_Line (Prefix & "Edited channel post: ");
            Put (Update.Edited_Channel_Post, Tab + 2);
            
            Put_Line (Prefix & "-----");
        end if;
    end Put;
    
    procedure Put (Message : Message_Type; Tab : Natural := 0) is
        Prefix : String := Tab * " ";
    begin
        if Message.Invalid then
            Set_Colour (Red);
            Put_Line (Prefix & "Message is empty or invalid");
            Reset_All;
        else
            Bold;
            Put (Prefix & "**** Message ");
            Put_Emoji (16#1F4AC#);
            Put_Line (":");
            Reset_All;
            
            Put_Line (Prefix & "Message id: " & Message.Message_Id'Image);
            
            Put_Line (Prefix & "Text: ");
            Set_Colour (Yellow);
            Wwio.Put_Line (To_Wide_Wide_String (Message.Text));
            Reset_All;
            
            Put (Prefix & "Date: " & Message.Date'Image & " (");
            Wwio.Put (To_Wide_Wide_String 
                        (Epoch_Seconds_To_String (Message.Date)));
            Put_Line (")");
            
            Put_Line (Prefix & "From: ");
            Put (Message.From, Tab + 2);

            Put_Line (Prefix & "Chat: ");        
            Put (Message.Chat, Tab + 2);
            
            Put_Line (Prefix & "-- End Message data");    
        end if;
    end Put;
    
    procedure Put (Chat : Chat_Type; Tab : Natural := 0) is
        Prefix : String := Tab * " ";
    begin
        if Chat.Invalid then
            Set_Colour (Red);
            Put_Line (Prefix & "Chat data is empty or invalid");
            Reset_All;
        else
            Bold;
            Put (Prefix & "**** Chat ");
            Put_Emoji (16#1f5e3#);
            Put_Variation;
            Put_Line (":");
            Reset_All;
            
            Put_Line (Prefix & "Chat id: " & Chat.Id'Image);
            Put (Prefix);
            Wwio.Put_Line ("Chat type: " & 
                             Us2wws (Chat.Chat_Type));
            Put (Prefix);
            Wwio.Put_Line ("Title: " & 
                             Us2wws (Chat.Title));
            Put (Prefix);
            Wwio.Put_Line ("Username: " & 
                             Us2wws (Chat.Username));
            Put (Prefix);
            Wwio.Put_Line ("First name: " & 
                             Us2wws (Chat.First_Name));
            Put (Prefix);
            Wwio.Put_Line ("Last name: " & 
                             Us2wws (Chat.Last_Name));
            Put (Prefix);
            Wwio.Put_Line ("Description: " & 
                             Us2wws (Chat.Description));
            Put (Prefix);
            Wwio.Put_Line ("Invite link: " &
                             Us2wws (Chat.Invite_Link));
            Put_Line (Prefix & "Pinned message:");
            
            Put_Line (Prefix & "Slow mode delay: " 
                        & Chat.Slow_Mode_Delay'Image);
            
            Put (Prefix);
            Wwio.Put_Line ("Sticker set name: " & 
                             Us2wws(Chat.Sticker_Set_Name));
            
            Put_Line (Prefix & "Can set sticker set: " & 
                        Chat.Can_Set_Sticker_Set'Image);
            
            if Chat.Pinned_Message /= null then
                Put (Chat.Pinned_Message.all, Tab + 2);
            else
                Put_Line (Prefix & " Undefined");
            end if;

            Put_Line (Prefix & "-- End Chat data");
        end if;
    end Put;
end Telegram.Puts;
