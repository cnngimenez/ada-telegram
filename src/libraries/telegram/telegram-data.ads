-- telegram-data.ads --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Calendar;
use Ada.Calendar;
with Ada.Containers.Vectors;
use Ada.Containers;

with League.Strings;
use League.Strings;
with League.Json.Documents;
use League.Json.Documents;
with League.Json.Objects;
use League.Json.Objects;
with League.Json.Arrays;
use League.Json.Arrays;

--
--  Common data structures and their convertion functions from its JSON
--  representation.
--
package Telegram.Data is
    
    type User_Type (Invalid : Boolean := False) is record
        case Invalid is
           when True =>
             null;
           when False =>
               Id : Long_Long_Integer;
               Is_Bot : Boolean;
               First_Name : Universal_String;
               Last_Name : Universal_String;
               Username : Universal_String;
               Language_Code : Universal_String;
               Can_Join_Groups : Boolean;
               Can_Read_All_Group_Messages : Boolean;
               Supports_Inline_Queries : Boolean;
        end case;
    end record;
    
    --  Convert a JSON type into User_Type
    function User_From_Json (Jdoc : Json_Document) return User_Type;
    function User_From_Json (Jobj : Json_Object) return User_Type;
    function From_Json (Jdoc : Json_Document) return User_Type
      renames User_From_Json;
    function From_Json (Jobj : Json_Object) return User_Type
      renames User_From_Json;
    
    type Message_Type;
    type Message_Access is access Message_Type;
    
    type Chat_Type (Invalid : Boolean := False) is record
        case Invalid is
           when True =>
             null;
           when False => 
               Id : Long_Long_Integer;
               Chat_type : Universal_String; --  "type" field
               Title : Universal_String;
               Username : Universal_String;
               First_Name : Universal_String;
               Last_Name : Universal_String;
               --  Photo : ChatPhoto;
               Description : Universal_String;
               Invite_Link : Universal_String;
               
               Pinned_Message : Message_Access;  
               --  Must be an access type.
               --  Permissions : ChatPermissions;
               
               Slow_Mode_Delay : Integer;
               Sticker_Set_Name : Universal_String;
               Can_Set_Sticker_Set : Boolean;
        end case;
        end record;
    
    function Chat_From_Json (Jdoc : Json_Document) return Chat_Type;
    function Chat_From_Json (Jobj : Json_Object) return Chat_Type;
    function From_Json (Jdoc : Json_Document) return Chat_Type
      renames Chat_From_Json;
    function From_Json (Jobj : Json_Object) return Chat_Type
      renames Chat_From_Json;
    
    type Message_Type (Invalid : Boolean := False) is record
        case Invalid is
           when True =>
             null;
           when False => 
               Message_Id : Long_Long_Integer;
               From : User_Type;
               Date : Integer;
               Chat : Chat_Type;
               --  forward_from : User;
               --  Forward_From_Chat : Chat;
               --  Forward_From_Message_Id : Integer;
               --  Forward_Signature : Universal_String;
               --  Forward_Sender_Name : Universal_String;
               --  Forward_Date : Integer;
               --  Reply_To_Message : Message;
               --  Edit_Date : Integer;
               --  Media_Group_Id : Universal_String;
               --  Author_Signature : Universal_String;
               Text : Universal_String;
               --  Entities : Array of MessageEntity;
               --  Caption_Entities : Array of MessageEntity;
               --  Audio : Audio;
               --  Document : Document;
               --  Animation : Animation;
               --  Game : Game;
               --  Photo : Array of PhotoSize;
               --  Sticker : Sticker;
               --  Video : Video;
               --  Voice : Voice;
               --  Video_Note : VideoNote;
               --  Caption : Universal_String;
               --  Contact : Contact;
               --  Location : Location;
               --  Venue : Venue;
               --  Poll : Poll;
               --  New_Chat_Members : Array of User;
               --  Left_Chat_Member : User;
               --  New_Chat_Title : Universal_String;
               --  New_Chat_Photo : Array of PhotoSize;
               --  Delete_Chat_Photo : True;
               --  Group_Chat_Created : True;
               --  Supergroup_Chat_Created : True;
               --  Channel_Chat_Created : True;
               --  Migrate_To_Chat_Id : Integer;
               --  Migrate_From_Chat_Id : Integer;
               --  Pinned_Message : Message;
               --  Invoice : Invoice;
               --  Successful_Payment : SuccessfulPayment;
               --  Connected_Website : Universal_String;
               --  Passport_Data : PassportData;
               --  Reply_Markup : InlineKeyboardMarkup;
        end case;
    end record;
    
    function Message_From_Json (Jdoc : Json_Document) return Message_Type;
    function Message_From_Json (Jobj : Json_Object) return Message_Type;
    function From_Json (Jdoc : Json_Document) return Message_Type
      renames Message_From_Json;
    function From_Json (Jobj : Json_Object) return Message_Type
      renames Message_From_Json;
    
    type Update_Type (Invalid : Boolean := False) is record
        case Invalid is
           when True => 
               null;
           when False => 
                 Update_Id : Long_Long_Integer;
                 Message : Message_Type;
                 Edited_Message : Message_Type;
                 Channel_Post : Message_Type;
                 Edited_Channel_Post : Message_Type;
                 --  Inline_Query : Inline_Query_Type;
                 --  Chosen_Inline_Result : Chosen_Inline_Result_Type;
                 --  Callback_Query : Callback_Query_Type;
                 --  Shipping_Query : Shipping_Query_Type;
                 --  Pre_Checkout_Query : Pre_Checkout_Query_Type;
                 --  Poll : Poll_Type;
                 --  Poll_Answer : Poll_Answer_Type;
        end case;
    end record;
    
    function Update_From_Json (Jdoc : Json_Document) return Update_Type;
    function Update_From_Json (Jobj : Json_Object) return Update_Type;
    function From_Json (Jdoc : Json_Document) return Update_Type
      renames Update_From_Json;
    function From_Json (Jobj : Json_Object) return Update_Type 
      renames Update_From_Json;
    
    package Update_Vectors is new Vectors 
      (Element_Type => Update_Type,
       Index_Type => Positive);
    
    function Update_Array_From_Json (Jdoc : Json_Document) 
                                    return Update_Vectors.Vector;
    function Update_Array_From_Json (Jobj : Json_Object) 
                                    return Update_Vectors.Vector;
    
    --
    --  Translate the JSON date type into an Ada.Calendar.Time.
    --
    function Epoch_Seconds_To_Time (Date : Integer) return Time;
    function Epoch_Seconds_To_String (Seconds : Integer) 
                                     return Universal_String;
    
    type Inline_Button_Type is tagged record
        Text : Universal_String;
        Url : Universal_String;
        --  login_url : Login_Url_type;
        Callback_Data : Universal_String; --  must be 1 to 64 bytes;
        --  switch_inline_query : string;
        --  switch_inline_query_current_chat : string;
        --  callback_game : Callback_Game_Type;
        --  pay : boolean
    end record;
       
    --  package Inline_Row_Vectors is new Vectors 
    --    (Element_Type => Inline_Button_Type,
    --     Index_Type => Positive);
    
    package Inline_Keyboard_Vectors is new Vectors
      (Element_Type => Inline_Button_Type,
       Index_Type => Positive);
    
    function Inline_Keyboard_To_Json_Array
      (Keyboard : Inline_Keyboard_Vectors.Vector) return Json_Array;
    
    --
    --  Invalid data
    --
    
    Invalid_User : constant User_Type := (Invalid => True);
    Invalid_Chat : constant Chat_Type := (Invalid => True);
    Invalid_Message : constant Message_Type := (Invalid => True);
    Invalid_Update : constant Update_Type := (Invalid => True);
    Invalid_Inline_Keyboard : constant Inline_Keyboard_Vectors.Vector
      := Inline_Keyboard_Vectors.Empty_Vector;
    
end Telegram.Data;
