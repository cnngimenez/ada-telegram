-- telegram-puts.ads --- 

-- Copyright 2020 cnngimenez
--
-- Author: cnngimenez

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Telegram.Data;
use Telegram.Data;

--
--  Print telegram data structures into standard input/output.
--
package Telegram.Puts is

    --  Print the user type into standard output.
    procedure Put (User : User_Type; Tab : Natural := 0);
    
    --  Print an Update type into standard output.
    procedure Put (Update : Update_Type; Tab : Natural := 0);
    
    procedure Put (Message : Message_Type; Tab : Natural := 0);
    
    procedure Put (Chat : Chat_Type; Tab : Natural := 0);
    
end Telegram.Puts;
