--  telegram-bots.adb ---

--  Copyright 2020 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with League.Json.Documents;
use League.Json.Documents;
with League.Characters;
use League.Characters;

--  with Ada.Text_Io;
--  use Ada.Text_Io;

package body Telegram.Bots is

    procedure Start_Loop is
        Last_Offset : Long_Long_Integer := 0;
        Json_Updates : Json_Document;
        
        Updates : Update_Vectors.Vector;
        
        procedure Process_Updates is
            Update : Update_Type;
            
            use Update_Vectors;
            Length : Integer := Integer (Updates.Length);
        begin
            for I in 1 .. Length loop
                Update := Updates (I);
                
                if Last_Offset < Update.Update_Id then
                    --  Put ("-- Last offset: ");
                    --  Put (Last_Offset'Image);
                    Last_Offset := Update.Update_Id;
                    --  Put (" -> ");
                    --  Put_Line (Last_Offset'Image);
                end if;
                
                Update_Received (Connection, Update);
                
                if Update.Message.Text (1) = '/' then
                    Command_Received (Connection, Update);
                end if;
            end loop;
        end Process_Updates;
        
    begin
        loop             
            --  Put_Line ("Getting updates");
            --  Put ("last_offset +1 = ");
            --  Put_Line (Long_Long_Integer'Image (Last_Offset + 1));
            Get_Updates (Connection, 
                         Offset => Last_Offset + 1,
                         Json => Json_Updates);
            
            Updates := Update_Array_From_Json (Json_Updates);
            
            Process_Updates;
            
            Updates.Clear;
            
            delay Duration(Timeout);
        end loop;
    end Start_Loop;
    
    
    
end Telegram.Bots;
