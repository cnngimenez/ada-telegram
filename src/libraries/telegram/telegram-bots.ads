--  telegram-bots.ads ---

--  Copyright 2020 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Telegram.Api_Methods;
use Telegram.Api_Methods;
with Telegram.Data;
use Telegram.Data;

generic
    Connection : Connection_Type;
    
    -- Seconds per each getUpdate call and action.
    Timeout : Natural := 2; 
    with procedure Update_Received (Connection : Connection_Type;
                                    Update : Update_Type) is null; 
    with procedure Command_Received (Connection : Connection_Type;
                                     Update : Update_Type) is null;
package Telegram.Bots is
    
    procedure Start_Loop;
    
end Telegram.Bots;
